﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float speed;
    private int damage;
    private Vector2 direction;

    private void Start()
    {
        Destroy(gameObject, 10);
    }
    public void SetParams(int damage, float speed, Vector2 direction)
    {
        this.speed = speed;
        this.direction = direction;
        this.damage = damage;
        transform.up = direction;
    }
    void FixedUpdate()
    {
        transform.position = transform.position + (Vector3)direction * speed * Time.fixedDeltaTime;
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        var hp = collision.gameObject.GetComponent<Hp>();
        if (hp != null)
            hp.OnHit(damage);
        Destroy(gameObject);
    }
}
