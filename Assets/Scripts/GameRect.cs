﻿using UnityEngine;

public class GameRect
{
    public Rect rect;

    public Vector2 Inscribe(Vector2 v)
    {
        if (v.x < rect.xMin)
        {
            v.x = rect.xMin;
        }
        if (v.x > rect.xMax)
        {
            v.x = rect.xMax;
        }
        if (v.y < rect.yMin)
        {
            v.y = rect.yMin;
        }
        if (v.y > rect.yMax)
        {
            v.y = rect.yMax;
        }
        return v;
    }
}