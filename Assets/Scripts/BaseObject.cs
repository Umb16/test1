﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseObject : MonoBehaviour
{
    protected float speed = 5;

    public void SetSpeed(float value)
    {
        speed = value;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer("projectile"))
            speed = 0;
    }
}
