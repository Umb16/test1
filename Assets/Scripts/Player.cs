﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : BaseObject
{
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private float bulletSpeed;
    private int damage;
    private Transform target;
    private Vector2 direction;
    private GameRect gameRect;

    public void SetParams(Transform target, int damage, float speed, GameRect rect)
    {
        this.target = target;
        this.damage = damage;
        gameRect = rect;
        SetSpeed(speed);
    }
    void Update()
    {
        RotateToTarget();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            CreateBullet();
        }

        DirectionControls();
    }

    private void DirectionControls()
    {
        direction = Vector2.zero;
        if (Input.GetKey(KeyCode.W))
            direction.y = 1;
        if (Input.GetKey(KeyCode.S))
            direction.y = -1;
        if (Input.GetKey(KeyCode.A))
            direction.x = -1;
        if (Input.GetKey(KeyCode.D))
            direction.x = 1;
        direction.Normalize();
    }

    private void CreateBullet()
    {
        var newBullet = Instantiate(bulletPrefab, transform.position + transform.up, Quaternion.identity);
        newBullet.GetComponent<Bullet>().SetParams(damage, bulletSpeed, transform.up);
    }

    private void RotateToTarget()
    {
        if (target != null)
        {
            transform.up = target.position - transform.position;
        }
    }

    void FixedUpdate()
    {
        direction *= speed * Time.fixedDeltaTime;
        Vector3 newPos = transform.position + (Vector3)direction;
        transform.position = gameRect.Inscribe(newPos);
    }
}
