﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehevior : BaseObject
{
    enum MoveState
    {
        Wait,
        Move,
        MoveToHome
    }

    private float moveDistance;
    private GameRect gameRect;

    private float waitTime;
    private Vector2 homePosition;
    private Vector2 moveTo;
    private Vector2 direction;
    private MoveState state = MoveState.Wait;

    public void SetParams(float moveDistance, float wait, GameRect rect)
    {
        this.moveDistance = moveDistance;
        gameRect = rect;
        waitTime = wait + Time.time;
    }
    private void Start()
    {
        homePosition = transform.position;

    }
    void Update()
    {
        if (waitTime < Time.time && state == MoveState.Wait)
        {
            SetNewDestination(GetNewPoint());
            state = MoveState.Move;
        }
    }

    void FixedUpdate()
    {
        if (state == MoveState.Wait)
            return;
        transform.position = transform.position + (Vector3)direction * speed * Time.fixedDeltaTime;
        if (Vector3.Distance(transform.position, moveTo) < speed * Time.fixedDeltaTime)
        {
            if (state == MoveState.Move)
            {
                SetNewDestination(homePosition);
                state = MoveState.MoveToHome;
            }
            else
            if (state == MoveState.MoveToHome)
            {
                state = MoveState.Wait;
            }
        }
    }

    void SetNewDestination(Vector2 point)
    {
        moveTo = point;
        direction = (Vector3)moveTo - transform.position;
        direction.Normalize();
    }

    Vector2 GetNewPoint()
    {
        Vector2 newPoint = new Vector2(Random.value - .5f, Random.value - .5f).normalized * moveDistance;
        return gameRect.Inscribe(newPoint);
    }
}
