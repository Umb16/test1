﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class StandartSettings : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private int playerDamage;
    [SerializeField]
    private float enemyMoveDistance;
    [SerializeField]
    private float enemyStartWait;
    [SerializeField]
    private GameObject playerPrefab;
    [SerializeField]
    private GameObject enemyPrefab;

    GameRect gameRect = new GameRect();
    private int screenWidth;
    private int screenHeight;

    void Start()
    {
        CalcGameRect();
        var enemy = Instantiate(enemyPrefab);
        enemy.GetComponent<BaseObject>().SetSpeed(speed);
        enemy.GetComponent<EnemyBehevior>().SetParams(enemyMoveDistance, enemyStartWait, gameRect);
        enemy.transform.position = gameRect.rect.center;

        var player = Instantiate(playerPrefab);
        player.GetComponent<Player>().SetParams(enemy.transform, playerDamage, speed, gameRect);
        player.transform.position = Vector3.up* (gameRect.rect.yMax - 1);
    }

    void Update()
    {
        if (screenWidth != Screen.width || screenHeight != Screen.height)
        {
            screenWidth = Screen.width;
            screenHeight = Screen.height;
            CalcGameRect();
        }
    }

    void CalcGameRect()
    {
        Vector3 point = Camera.main.ScreenToWorldPoint(new Vector3(0, 0));
        gameRect.rect.xMin = point.x;
        gameRect.rect.yMin = point.y;
        point = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
        gameRect.rect.xMax = point.x;
        gameRect.rect.yMax = point.y;
    }
}
