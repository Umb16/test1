﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hp : MonoBehaviour
{
    [SerializeField]
    private int value;
    [SerializeField]
    private GameObject particles;

    public void OnHit(int damage)
    {
        value -= damage;
        if (value <= 0)
            OnDie();
    }
    void OnDie()
    {
        Destroy(gameObject);
        Instantiate(particles, transform.position, Quaternion.identity);
    }
}
